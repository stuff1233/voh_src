// ======================================================================
//
// ValidateAccountMessage.cpp
// copyright (c) 2001 Sony Online Entertainment
//
// ======================================================================

#include "serverNetworkMessages/FirstServerNetworkMessages.h"
#include "serverNetworkMessages/ValidateAccountMessage.h"

// ======================================================================

ValidateAccountMessage::ValidateAccountMessage (StationId stationId, unsigned int track) :
	GameNetworkMessage("ValidateAccountMessage"),
	m_stationId(stationId),
	m_track(track)
{
	addVariable(m_stationId);
	addVariable(m_track);
}

// ----------------------------------------------------------------------

ValidateAccountMessage::ValidateAccountMessage (Archive::ReadIterator & source) :
	GameNetworkMessage("ValidateAccountMessage"),
	m_stationId(),
	m_track()
{
	addVariable(m_stationId);
	addVariable(m_track);

	AutoByteStream::unpack(source);
}

// ----------------------------------------------------------------------

ValidateAccountMessage::~ValidateAccountMessage ()
{
}

// ----------------------------------------------------------------------

unsigned int ValidateAccountMessage::getTrack() const
{
	return m_track.get();
}

// ======================================================================
